﻿using System;

/*
 * Rikk Shimizu
 * This is my own work.
 */

namespace Exercise7
{
    class exercise7_MeasurementConversion
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }//ends main default

        //Question 1
        public void showSum(int x, int y)
        {
            //would actually "display" sum of two ints
            Console.WriteLine(x + y);
        }//ends question 1

        //Question 2
        public double averageOfFiveDoubles(double first, double second, double third, double fourth, double fifth)
        {
            //faux body to show return
            double average = 0;

            return average;
        }//ends question 2

        //Question 3
        public int sumOfTwoRandomInts()
        {
            //body to show a return for two random ints
            Random randomNumberGenerator = new Random();
            int sumOfRandomInts; //holder of the sum to return

            //create two random ints up to 10,000 can always increase...
            int randomOne = randomNumberGenerator.Next(10000);
            int randomTwo = randomNumberGenerator.Next(10000);


            sumOfRandomInts = randomOne + randomTwo;

            return sumOfRandomInts;
        }//ends question 3

        //Question 4
        public bool isDivisibleByThree(int first, int second, int third)
        {
            //faux body to show return concept
            bool divisibleByThree = false;

            return divisibleByThree;
        }//ends question 4

        //Question 5
        public void displayFewestCharacters(string comparableOne, string comparableTwo)
        {
            //faux body to show a "display" 
            string fewestCharacters = "";
            Console.WriteLine(fewestCharacters);
        }//ends question 5

        //Question 6
        public double largestDoubleInArray(double[] arrayOfDoubles)
        {
            //faux body to show return for largest value in the double array
            double largestDoubleInArray = 0.0;

            return largestDoubleInArray;
        }//ends question 6

        //Question 7
        public int[] generateFiftyIntsIntoArray()
        {
            //body to represent building an array with 50 slots for integers
            int[] arrayOfInts = new int[50];

            return arrayOfInts;
        }//ends question 7
        
        //Question 8
        public bool valueEquivalencyCheck(bool varOne, bool varTwo)
        {
            //body to represent return of true false for equivalency
            bool equivalent = false;

            if(varOne == varTwo)
            {
                equivalent = true;
            }//ends if

            return equivalent;
        }//ends question 8

        //Question 9
        public double productOfIntAndDouble(int x, double y)
        {
            //body to show return of a double for the product, to keep decimal value of the double
            double product = x * y;

            return product;
        }//ends question 9

        //Question 10
        public int averageOfTwoDimensionalArray(int[,] entries)
        {
            //faux body to represent return of an int for the average of all ints
            int averageOfEntries = 0;

            return averageOfEntries;
        }//ends question 10

    }//ends class
}//ends namespace
